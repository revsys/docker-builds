

In cases where git updates are necessary more efficiently than package 
maintainer release updated packages:


The provided target takes the following environment variables to build the 
docker CLI:

 * `GIT_VERSION` (N.NN.N)
 * `PKG_PROXY`: (str) url of a local http proxy


Usage example:

```

FROM registry.gitlab.com/revsys/docker-builds/git-alpine-3.6:2.14.2 as git-upgrade

FROM gitlab/gitlab-runner:alpine-v10.1.0

RUN apk --no-cache del git ;\
    apk --no-cache add libcurl libssh2 expat pcre2 ;\
    apk --no-cache upgrade

COPY --from=git-upgrade /usr/local /usr/local

```

