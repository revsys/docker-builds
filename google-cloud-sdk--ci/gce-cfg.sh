#!/bin/bash

gcloud auth activate-service-account --key-file <( echo $GOOGLE_KEY )
gcloud config set compute/zone ${ZONE}
gcloud config set project ${PROJECT}
gcloud container clusters get-credentials ${PROJECT_ID}

helm list

