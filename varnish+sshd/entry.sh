#!/bin/bash

chmod 0700 /usr/local/etc/{tls,converted}
chmod 0600 /usr/local/etc/converted/*
exec /usr/local/sbin/sshd -D $@
